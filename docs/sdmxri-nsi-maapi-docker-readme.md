# About this Image

The official NSI WS image of SIS-CC .Stat Suite, configured to be ready for using in a [.Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/) installation.
The image built from official repositories of Eurostat SDMXRI (NSI) SDMX.

The *siscc/sdmxri-nsi-maapi* NSI WS image provides the following features on top of the ["vanilla" version of NSI WS](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored):
- configured to use OpenId connect authentication;
- configured to use authorization rules defined in .Stat Suite;
- includes ESTAT's [MAAPI (command line) tool](https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored/-/tree/master/src/Estat.Sri.Mapping.Tool?ref_type=heads), used to initialize/update the mapping store database objects. The tool requires an empty database with a user login defined.

This version of the siscc/sdmxri-nsi-maapi image supports MS SQL and MariaDB database servers only.

Source repository of the image: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws

# Environment Variables

You can use the following environment variables to configure the sdmxri-nsi-maapi service:
- Mapping store database configuration of NSI WS
  - ***SQL_SERVER*** - SQL server instance hosting the mapping store database
  - ***SQL_DATABASE*** - The name of mapping store database
  - ***SQL_USER*** - The application user
  - ***SQL_PASSWORD*** - The passord for the application user
  - ***SQL_PROVIDER*** - Specify the database provider to be used. "System.Data.SqlClient" or "MySql.Data.MySqlClient" etc. The default value is "System.Data.SqlClient".
- MAAPI tool variables:
  - ***MA_SQL_USER*** - Privileged user (e.g. sa) that has the permission to create/drop/alter db objects on the database defined by *SQL_SERVER* and *SQL_DATABASE* parameters
  - ***MA_SQL_PASSWORD*** - Password of privileged user
  - ***MA_ALWAYS_RESET*** - Y/N, specifies if the mapping store database should reset (Y) or persist (N) all the changes done to the artifacts, when the container is restarted
  - ***MA_DB_VERSION*** (Optional) - Most of the cases should not be used. Specifies the mapping store database version to be used, if not the latest is reuqired. Use this parameter only when you know what you are doing.
- Authorization configuration
  - ***authorization__enabled*** (Optional) - true/false, determines if UserAuthorizationRulesMiddlerwareBuilder is activated for authorization. The default value for this docker image is "true", which normally should not be changed.
  - ***authorization__method*** (Optional) - "dotstat" - authorization uses .Stat Suite's authorization rules. The default value for this docker image is "dotstat", which normally should not be changed.
  - ***authorization__PrincipalFrom*** (Optional) - "context", the principal used at authorization should be taken from context object. The default value for this docker image is "context", which normally should not be changed.
  - ***mappingStore__Id__Default*** - the unique identifier of the data space of the NSI WS instance (also used at authorization rules), e.g. "design"
  - ***mappingStore__Id__FromQueryParameter*** (Optional) - "Never", a parameter controlling whether store id (i.e. data space id) could be provided in GET Query parameter. The default value for this docker image is "Never", which normally should not be changed.
  - ***DotStatSuiteCoreCommonDbConnectionString*** - the connection string to .Stat Suite's Common databse with authorization rules, e.g. 
  - ***DbType*** - Specifies databse to be used, default "SqlServer". Use "MariaDb" when using Maria DB.
Server=db;Database=CommonDb;User=testLoginCommon;Password=testLogin(!)Password;
- NSI web service variables:
  - ***INSERT_NEW_ITEM_SCHEME_VALUES*** (Optional) - "true/false", determines if the application is allowed to add new item scheme values, e.g. new codes to an existing code list
  - ***SENDER_ID*** - the ID of the sender applied in the header of data messages, e.g. "Dissemination-DotStat"
- Further applicable NSI WS configuration options are described in the documentation of NSI WS:
  - Authentication: https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/master/doc/openid-middleware.md#configuration
  - Maximum size of the submited SDMX messages: https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/master/doc/CONFIGURATION.md?ref_type=heads#maximum-size-of-the-submited-sdmx-messages  
  - Optional Logging to the Google cloud using log4net: https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/master/doc/CONFIGURATION.md?ref_type=heads#optional-logging-to-the-google-cloud-using-log4net
  - Other JSON configuration elements: https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/master/doc/CONFIGURATION.md?ref_type=heads#other-configuration-in-json-files
 
  
# How to use this image

Start siscc/sdmxri-nsi-maapi (NSI WS) service instance listening on port 81 using the latest release, where the content of structure database is preserved. 

IMPORTANT NOTE: If you are using PowerShell on Windows to run these commands, use double quotes instead of single quotes.

Example:
```sh
docker run \
-e 'auth__enabled=true' \
-e 'auth__allowAnonymous=true' \
-e 'auth__authority=http://keycloak:8080/auth/realms/${KEYCLOAK_REALM}' \
-e 'auth__clientId=stat-suite ' \
-e 'auth__requireHttps=false' \
-e 'auth__validateIssuer=false' \
-e 'auth__showPii=true' \
-e 'mappingStore__Id__Default=demo-design' \
-e 'DotStatSuiteCoreCommonDbConnectionString=Server=db;Database=CommonDb;User=testLoginCommon;Password=testLogin(!)Password;' \
-e 'SQL_SERVER=db' \
-e 'SQL_DATABASE=DesignStructDb' \
-e 'SQL_USER=testLoginDesignStruct' \
-e 'SQL_PASSWORD=testLogin(!)Password' \
-e 'SQL_PROVIDER=System.Data.SqlClient' \
-e 'MA_SQL_USER=sa' \
-e 'MA_SQL_PASSWORD=My-Mssql-Pwd-123' \
-e 'MA_ALWAYS_RESET=N' \
-e 'INSERT_NEW_ITEM_SCHEME_VALUES=true' \
-e 'SENDER_ID=.Stat-v8-demo-design' \
-e 'enableReleaseManagement=true ' \
-p 81:80 \
siscc/sdmxri-nsi-maapi:master
```

