ARG NSI_IMAGE=siscc/sdmxri-nsi:latest
ARG NUGETFEED=https://api.nuget.org/v3/index.json

FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build
WORKDIR /app

#---- maapi.net ------------------------------------
COPY maapi.net ./maapi.net

ENV MSBuildEmitSolution=1
RUN dotnet publish /app/maapi.net/src/Estat.Sri.Mapping.Tool -c Release -r linux-musl-x64 -o /out -nowarn:NU1605,CS1591,CS1570,CS1572,CS1573,CS1574

#---- dotstat authorization plugin ------------------------------------
COPY authorization.net ./authorization.net

ENV MSBuildEmitSolution=1
RUN dotnet restore /app/authorization.net/src/estat.sri.ws.auth.dotstat -r linux-musl-x64 -p:WarningsAsErrors= -nowarn:NU1605,CS1591,CS1570,CS1572,CS1573,CS1574
RUN dotnet publish /app/authorization.net/src/estat.sri.ws.auth.dotstat --no-restore -c Release -o /authorization.out -p:WarningsAsErrors= -nowarn:NU1605,CS1591,CS1570,CS1572,CS1573,CS1574

#---- dotstat authorization plugin ------------------------------------
COPY transferservice.net ./transferservice.net

ENV MSBuildEmitSolution=1
RUN dotnet restore /app/transferservice.net/DotStat.Transfer.Messaging.Producer -r linux-musl-x64 -p:WarningsAsErrors= -nowarn:NU1605,CS1591,CS1570,CS1572,CS1573,CS1574
RUN dotnet publish /app/transferservice.net/DotStat.Transfer.Messaging.Producer -c Release -o /messaging.out -p:WarningsAsErrors= -nowarn:NU1605,CS1591,CS1570,CS1572,CS1573,CS1574

#---- runtime ---------------------------------------------------------
FROM $NSI_IMAGE AS runtime
WORKDIR /app

#-- copy maapi tool build
COPY --from=build /out ./maapi-tool

# Copy dotstat authorization dll 
COPY --from=build /authorization.out/DotStat.* ./Plugins/
COPY --from=build /authorization.out/estat.sri.ws.auth.dotstat.* ./Plugins/

# Copy dotstat messaging 
COPY --from=build /messaging.out/RabbitMQ.Client.dll ./Plugins/
COPY --from=build /messaging.out/DotStat.Common.dll ./Plugins/
COPY --from=build /messaging.out/DotStat.Transfer.Messaging.* ./Plugins/

#Copy config file templates (nsi ws and maapi tool) from local directory 
COPY config-templates ./config-templates

#Copy generic configuration valid to all instances to NSI's config folder
COPY config ./config

#Copy entry point shell script
COPY scripts/entrypoint.sh .

RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]