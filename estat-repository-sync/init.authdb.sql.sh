#!/bin/sh 

rm -rf authdb.sql.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/authdb.sql.git authdb.sql.mirrored

#git remote set-url origin https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/authdb.sql.git
git -C ./authdb.sql.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/authdb.sql.mirrored.git

git -C ./authdb.sql.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./authdb.sql.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./authdb.sql.mirrored config --local --add remote.origin.mirror true

git -C ./authdb.sql.mirrored config --local -l
