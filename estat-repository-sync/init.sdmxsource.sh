#!/bin/sh 

rm -rf sdmxsource.net.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/sdmxsource.net.git sdmxsource.net.mirrored

git -C ./sdmxsource.net.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/sdmxsource.net.mirrored.git

git -C ./sdmxsource.net.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./sdmxsource.net.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./sdmxsource.net.mirrored config --local --add remote.origin.mirror true

git -C ./sdmxsource.net.mirrored config --local -l