#!/bin/sh 

rm -rf dr.net.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/dr.net.git dr.net.mirrored

git -C ./dr.net.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/dr.net.mirrored.git

git -C ./dr.net.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./dr.net.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./dr.net.mirrored config --local --add remote.origin.mirror true

git -C ./dr.net.mirrored config --local -l