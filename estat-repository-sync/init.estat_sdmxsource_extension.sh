#!/bin/sh 

rm -rf estat_sdmxsource_extension.net.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/estat_sdmxsource_extension.net.git estat_sdmxsource_extension.net.mirrored

git -C ./estat_sdmxsource_extension.net.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/estat_sdmxsource_extension.net.mirrored.git

git -C ./estat_sdmxsource_extension.net.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./estat_sdmxsource_extension.net.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./estat_sdmxsource_extension.net.mirrored config --local --add remote.origin.mirror true

git -C ./estat_sdmxsource_extension.net.mirrored config --local -l