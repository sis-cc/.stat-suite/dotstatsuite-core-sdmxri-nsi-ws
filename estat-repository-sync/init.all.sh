#!/bin/sh 
Echo "-> authdb.sql.mirrored"
./init.authdb.sql.sh

Echo "-> maapi.net.mirrored"
./init.maapi.sh

Echo "-> authorization.net.mirrored"
./init.authorization.sh

Echo "-> nsiws.net.mirrored"
./init.nsiws.sh

Echo "-> dr.net.mirrored"
./init.dr.sh

Echo "-> estat_sdmxsource_extension.net.mirrored"
./init.estat_sdmxsource_extension.sh

Echo "-> msdb.sql.mirrored"
./init.msdb.sql.sh

Echo "-> sdmxsource.net.mirrored"
./init.sdmxsource.sh

Echo "-> sr.net.mirrored"
./init.sr.sh

