#!/bin/sh 

rm -rf nsiws.net.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/nsiws.net.git nsiws.net.mirrored

#git remote set-url origin https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/nsiws.net.git
git -C ./nsiws.net.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored.git

git -C ./nsiws.net.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./nsiws.net.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./nsiws.net.mirrored config --local --add remote.origin.mirror true

git -C ./nsiws.net.mirrored config --local -l