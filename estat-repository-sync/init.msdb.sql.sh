#!/bin/sh 

rm -rf msdb.sql.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/msdb.sql.git msdb.sql.mirrored

git -C ./msdb.sql.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/msdb.sql.mirrored.git

git -C ./msdb.sql.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./msdb.sql.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./msdb.sql.mirrored config --local --add remote.origin.mirror true

git -C ./msdb.sql.mirrored config --local -l