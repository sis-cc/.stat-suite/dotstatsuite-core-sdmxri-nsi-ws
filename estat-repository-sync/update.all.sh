#!/bin/sh 
export GIT_SSL_NO_VERIFY=true

Echo "-> authdb.sql.mirrored"
git -C ./authdb.sql.mirrored --bare fetch
git -C ./authdb.sql.mirrored --bare push

Echo "-> maapi.net.mirrored"
git -C ./maapi.net.mirrored --bare fetch
git -C ./maapi.net.mirrored --bare push

Echo "-> authorization.net.mirrored"
git -C ./authorization.net.mirrored --bare fetch
git -C ./authorization.net.mirrored --bare push

Echo "-> nsiws.net.mirrored"
git -C ./nsiws.net.mirrored --bare fetch
git -C ./nsiws.net.mirrored --bare push

Echo "-> dr.net.mirrored"
git -C ./dr.net.mirrored --bare fetch
git -C ./dr.net.mirrored --bare push

Echo "-> estat_sdmxsource_extension.net.mirrored"
git -C ./estat_sdmxsource_extension.net.mirrored --bare fetch
git -C ./estat_sdmxsource_extension.net.mirrored --bare push

Echo "-> msdb.sql.mirrored"
git -C ./msdb.sql.mirrored --bare fetch
git -C ./msdb.sql.mirrored --bare push

Echo "-> sdmxsource.net.mirrored"
git -C ./sdmxsource.net.mirrored --bare fetch
git -C ./sdmxsource.net.mirrored --bare push

Echo "-> sr.net.mirrored"
git -C ./sr.net.mirrored --bare fetch
git -C ./sr.net.mirrored --bare push
