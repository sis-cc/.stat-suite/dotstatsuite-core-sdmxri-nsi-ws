stages:  
  - build
  - deploy-qa
  - deploy-staging
  - performance_test_qa #Trigger dotstatsuite-quality-assurance smoke performance tests
  - test # needed by Dependency-Scanning
  - security-test
  - dast
  
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: DAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml

variables:
  NSI_IMAGE: siscc/sdmxri-nsi
  MAAPI_IMAGE: siscc/sdmxri-nsi-maapi
  nsi_repository: https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored.git
  nsi_branch: 8.19.6
  maapi_repository: https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored.git
  maapi_branch: 8.19.6
  authdb_repository: ../authdb.sql.mirrored.git
  authorization_repository: https://gitlab.com/sis-cc/eurostat-sdmx-ri/authorization.net.mirrored.git
  authorization_branch: 8.19.6
  transferservice_repository: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer.git
  transferservice_branch: 16.2.0
  DS_EXCLUDED_ANALYZERS: "gemnasium-python,gemnasium-maven"

build:
  stage: build
  image: docker:git
  before_script:
    - COLOR='\033[93m'
    - NOCOLOR='\033[0m' # No Color  
    - |
      echo -e "NSIWS.NET"
      echo -e " - repository = $COLOR $nsi_repository $NOCOLOR"
      echo -e " - branch     = $COLOR $nsi_branch $NOCOLOR"
      echo -e "MAAPI.NET"
      echo -e " - repository = $COLOR $maapi_repository $NOCOLOR"
      echo -e " - branch     = $COLOR $maapi_branch $NOCOLOR"
      echo -e " - AUTHDB.SQL = $COLOR $authdb_repository $NOCOLOR"
      echo -e "AUTHORIZATION.NET" 
      echo -e " - repository = $COLOR $authorization_repository $NOCOLOR"
      echo -e " - branch     = $COLOR $authorization_branch $NOCOLOR"
      echo -e "TRANSFER SERVICE MESSAGING" 
      echo -e " - repository = $COLOR $transferservice_repository $NOCOLOR"
      echo -e " - branch     = $COLOR $transferservice_branch $NOCOLOR"
      echo -e "CI_CI_USER_NAME     = $COLOR $CI_USER_NAME $NOCOLOR"
      echo -e "CI_COMMIT_SHORT_SHA = $COLOR $CI_COMMIT_SHORT_SHA $NOCOLOR"
    - git config --global credential.helper cache
    - git clone -b $nsi_branch --single-branch  $nsi_repository nsi
    - git clone -b $authorization_branch --single-branch  $authorization_repository authorization.net
    - git clone -b $maapi_branch --single-branch $maapi_repository maapi.net
    - git clone -b $transferservice_branch --single-branch $transferservice_repository transferservice.net 
    - cd ./maapi.net/
    - git config --file=.gitmodules submodule.authdb.url $authdb_repository
    - git submodule update --init --single-branch
    - cd ../
    - docker login -u "$CI_USER_NAME" -p "$CI_USER_PWD"
  services:
    - docker:dind
  script:
    - cp -rf nsi-Dockerfile nsi/Dockerfile
    - export TAG=${nsi_branch}
    - docker build -f nsi/Dockerfile -t $NSI_IMAGE:$TAG nsi
    - echo -e "The following docker image has been built -> $NSI_IMAGE:$COLOR$TAG$NOCOLOR"
    - docker build -f maapi-Dockerfile --build-arg NSI_IMAGE=$NSI_IMAGE:$TAG -t $MAAPI_IMAGE:$TAG .
    - docker tag $MAAPI_IMAGE:$TAG $MAAPI_IMAGE:$TAG-$CI_COMMIT_SHORT_SHA 
    - docker push $NSI_IMAGE:$TAG
    - docker push $MAAPI_IMAGE:$TAG
    - docker push $MAAPI_IMAGE:$TAG-$CI_COMMIT_SHORT_SHA
    - |
      if [[ $CI_COMMIT_REF_NAME == "master" ]] || [[ $CI_COMMIT_REF_NAME == "develop" ]]; then
        docker tag $MAAPI_IMAGE:$TAG $MAAPI_IMAGE:$CI_COMMIT_REF_NAME
        docker tag $MAAPI_IMAGE:$TAG $MAAPI_IMAGE:latest
        docker push $MAAPI_IMAGE:$CI_COMMIT_REF_NAME
        docker push $MAAPI_IMAGE:latest
      fi
    - |
      echo -e "The following docker images have been created:"
      echo -e " - $NSI_IMAGE:$COLOR$TAG$NOCOLOR"
      echo -e " - $MAAPI_IMAGE:$COLOR$TAG$NOCOLOR"
      echo -e " - $MAAPI_IMAGE:$COLOR$TAG-$CI_COMMIT_SHORT_SHA$NOCOLOR"
    - |
      if [[ $CI_COMMIT_REF_NAME == "master" ]] || [[ $CI_COMMIT_REF_NAME == "develop" ]]; then
        echo -e " - $MAAPI_IMAGE:$COLOR$CI_COMMIT_REF_NAME$NOCOLOR"
        echo -e " - $MAAPI_IMAGE:${COLOR}latest$NOCOLOR"
      fi
  when: manual
  except:
    - schedules
  allow_failure: false
  artifacts:
    paths:
      - nsi

.template: &template
    image: google/cloud-sdk
    before_script:
        - echo ${KUBE_ACCOUNT_KEY} | base64 -di > key.json
        - echo deploy $NSI
    script:
        - export TAG=${nsi_branch}
        - gcloud auth activate-service-account --key-file=key.json
        - gcloud config set project oecd-228113
        - gcloud config set container/cluster oecd-core
        - gcloud config set compute/region europe-west1
        - gcloud container clusters get-credentials oecd-core --region europe-west1-b
        - kubectl get pods -n $NS -o wide
        - kubectl set image deployment/$NSI $NSI=$MAAPI_IMAGE:$TAG-$CI_COMMIT_SHORT_SHA -n $NS
        - kubectl set env deployment/$NSI GIT_HASH=$CI_COMMIT_SHA -n $NS
    environment:
        name: $NS
    when: manual
    except:
        - schedules
    tags:
        - kube
        - oecd

deploy-qa-nsi-reset:
    variables:
        NSI: nsi-reset
        NS: qa
    <<: *template
    stage: deploy-qa
    only:
        - develop

deploy-qa-nsi-stable:
    variables:
        NSI: nsi-stable
        NS: qa
    <<: *template
    stage: deploy-qa
    only:
        - develop

deploy-qa-mariadb-nsi-reset:
    variables:
        NSI: nsi-reset
        NS: qa-mariadb
    <<: *template
    stage: deploy-qa
    only:
        - develop

deploy-qa-mariadb-nsi-stable:
    variables:
        NSI: nsi-stable
        NS: qa-mariadb
    <<: *template
    stage: deploy-qa
    only:
        - develop

deploy-staging-nsi-design-oecd:
    variables:
        NSI: nsi-design-oecd
        NS: staging
    <<: *template
    stage: deploy-staging
    only:
        - master

deploy-staging-nsi-staging-oecd:
    variables:
        NSI: nsi-staging-oecd
        NS: staging
    <<: *template
    stage: deploy-staging
    only:
        - master

deploy-staging-nsi-stable-siscc:
    variables:
        NSI: nsi-stable-siscc
        NS: staging
    <<: *template
    stage: deploy-staging
    only:
        - master

deploy-staging-nsi-reset-siscc:
    variables:
        NSI: nsi-reset-siscc
        NS: staging
    <<: *template
    stage: deploy-staging
    only:
        - master

deploy-staging-mariadb-nsi-stable:
    variables:
        NSI: nsi-stable
        NS: staging-mariadb
    <<: *template
    stage: deploy-staging
    only:
        - master

deploy-staging-mariadb-nsi-reset:
    variables:
        NSI: nsi-reset
        NS: staging-mariadb
    <<: *template
    stage: deploy-staging
    only:
        - master

build-nuget-lock:
  stage: security-test
  image: mcr.microsoft.com/dotnet/sdk:6.0
  script:
    - dotnet restore nsi/NSIWebServices.sln --use-lock-file --lock-file-path $CI_PROJECT_DIR/nuget-lock/packages.lock.json
  artifacts:
    paths:
      - nuget-lock
  rules:
    - if:  $CI_PIPELINE_SOURCE == "schedule" || $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $DEPENDENCY_SCANNING_DISABLED == null || $DEPENDENCY_SCANNING_DISABLED != "true"
      when: always
  needs:
    - build

gemnasium-dependency_scanning:
  stage: security-test
  variables:
    DS_REMEDIATE: "false"
  dependencies:
    - build-nuget-lock
  rules:
    - if:  $CI_PIPELINE_SOURCE == "schedule" || $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never  
    - if: $DEPENDENCY_SCANNING_DISABLED == null || $DEPENDENCY_SCANNING_DISABLED != "true"
      when: always
  needs:
    - build-nuget-lock
    
container_scanning:
  stage: security-test
  variables:
    CS_IMAGE: $MAAPI_IMAGE:$CI_COMMIT_BRANCH
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
    - if: $CI_COMMIT_BRANCH == "master"    
    
dast:
  variables:
    DAST_WEBSITE: "https://nsi-qa-reset.siscc.org/swagger/v1/swagger.json"
    REVIEW_DISABLED: "true"
  rules:
    - if:  $CI_PIPELINE_SOURCE == "schedule"
      when: always